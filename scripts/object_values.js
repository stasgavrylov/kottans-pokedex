// Micro-polyfill for Object.values
Object.values = object => Object.keys(object).map(key => object[key])