(function()
{
  'use strict'

  // Improvised mixin for all declared type color variables
  var stylesheet = Object.values(document.styleSheets).find(sheet => sheet.href.includes('typecolors'))
  var rule = [].find.call(stylesheet.cssRules, rule => rule.selectorText == ":root")
  var colorVars = [].map.call(rule.style, name => name.slice(2))

  colorVars.forEach(type =>
  {
    let typeRule = `.${type} {--color: var(--${type})}`
    stylesheet.insertRule(typeRule, stylesheet.cssRules.length)
  })




  // "Global" DOM elements
  var loader = document.query('.loader')
  var filterForm = document.query('form')
  var filterList = filterForm.query('.types')

  // "Global" objects to hold info on pokemon stats and filter types
  var pokemonStats = new Map()
  var filterTypes = new Map()

  // URL to get first 12 pokemon records
  var nextQuery = '/api/v1/pokemon/?limit=12'

  // Register custom elements that will represent our structure
  var pokemonList = new (document.registerElement('pokemon-list',
  {
    prototype: Object.create(HTMLUListElement.prototype)
  }));
  document.query('section').prepend(pokemonList)

  var PokemonCard = document.registerElement('pokemon-card',
  {
    prototype: Object.create(HTMLLIElement.prototype)
  });





  // Function that will append 'Load more' button to request new pokemon records
  var addBtn = function()
  {
    var btn = document.createElement('a')
    btn.textContent = 'Load more'
    btn.classList.add('more')

    on(btn, 'click', event =>
    {
      event.preventDefault()
      loader.classList.add('pending')
      addRecords()
    })

    document.query('section').append(btn)
  }

  // Function to append popup that will display pokemon stats
  var addStatsScreen = function()
  {
    var statsScreen = document.createElement('dialog')
    statsScreen.classList.add('stats-screen')
    document.body.append(statsScreen)

    on(statsScreen, 'click', event =>
    {
      event.preventDefault()
      if (event.target.closest('.close'))
        statsScreen.close()
    })
  }

  // Function to append filters menu
  function revealFilterForm()
  {
    filterForm.classList.remove('hidden')

    on(filterList, 'click', event =>
    {
      if (event.target.matches('li'))
      {
        event.target.classList.toggle('selected')
        applyFilters()
      }
    })

    on(filterForm, 'change', applyFilters)
  }

  // Apply filters to the cards list.
  function applyFilters()
  {
    var filterType = document.query(':checked').value

    switch(filterType)
    {
      case 'union':
        filterByUnion()
        break
      case 'intersection':
        filterByIntersection()
        break
    }
  }

  // Filter all cards that do not have any of selected types
  function filterByUnion(ids)
  {
    var selectedFiltersById = ids || filterList.queryAll('.selected').reduce((arr, $li) =>
    {
      return arr.concat($li.dataset.ids.split(','))
    }, [])

    document.queryAll('pokemon-card').forEach(card =>
    {
      card.classList.toggle('hidden', selectedFiltersById.length &&
                                       !selectedFiltersById.includes(card.getAttribute('pkdx-id')))
    })
  }

  // Filter all cards that do not have all of selected types
  function filterByIntersection()
  {
    // Message to show if no results matched the filter
    var emptyList = document.query('.emptylist')
    emptyList.classList.add('hidden')

    var selectedFiltersByIdGroups = filterList.queryAll('.selected').map($li =>
    {
      return $li.dataset.ids.split(',')
    })

    if (selectedFiltersByIdGroups.length <= 1)
    {
      filterByUnion()
      return
    }

    var firstGroup = selectedFiltersByIdGroups.shift()
    var matchedIds = firstGroup.filter(id =>
    {
      return selectedFiltersByIdGroups.every(group => group.includes(id))
    })


    // set length to truthy value so that we hide all records in case if 0 found.
    if (!matchedIds.length)
    {
      matchedIds.length = 1
      emptyList.classList.remove('hidden')
    }

    filterByUnion(matchedIds)
  }

  // Add all controls from above to page
  var initControls = once(function()
  {
    addStatsScreen()
    addBtn()
    revealFilterForm()
  })


  // Append received list items to the list
  function addRecords()
  {
    fetch('https://pokeapi.co'+nextQuery).then(response =>
    {
      return response.json()
    }).then(responseBody =>
    {
      loader.classList.remove('pending')
      initControls()

      nextQuery = responseBody.meta.next
      var pokemonCards = parseRecords(responseBody.objects)

      pokemonList.append.apply(pokemonList, pokemonCards)
      applyFilters()

      // Insert received ids into filter list for faster search in future
      filterList.queryAll('li').forEach($li =>
      {
        $li.dataset.ids = filterTypes.get($li.dataset.type).join()
      })

    }).catch(reject =>
    {
      loader.classList.remove('pending')
      console.log('Rejected because of ' + reject)
    })
  }

  // Build a string of list items HTML from records object
  function parseRecords(records)
  {
    return Object.values(records)
                .map(pokemon =>
                {
                  savePokemonInfo(pokemon)
                  return buildCard(pokemon)
                })
  }

  // Helper to build necessary HTML structure
  function buildCard(record)
  {
    var id = record.pkdx_id + ''
    var itemTypes = []

    // Build markup for all pokemon's types
    var typesMarkup = record.types.map(typeObj =>
    {
      var type = typeObj.name

      // Save type and record id to filterTypes object
      itemTypes.push(type)
      if (filterTypes.has(type))
        filterTypes.get(type).push(id)
      else {
        filterTypes.set(type, [id])
        appendFilter(type)
      }

      // Build type markup for pokemon card
      var fontsizeClass = type.length > 7 ? 'smalltext' : ''
      return `<li class="${type} ${fontsizeClass}" data-type="${type}">${type}`
    }).join('')


    // Build pokemon card
    var card = new PokemonCard()
    card.setAttribute('pkdx-id', id)
    card.innerHTML = `
                        <img src="https://pokeapi.co/media/img/${id}.png" alt="${record.name}">
                        <p class="name">${record.name}</p>
                        <ul class="types">
                            ${typesMarkup}
                        </ul>
                      `

    return card
  }

  // Add filter type to the list of filters
  function appendFilter(type)
  {

    var filterMarkup = `<li class="${type}" data-type="${type}">${type}`
    filterList.insertAdjacentHTML('beforeend', filterMarkup)
  }

  // Save all received data to pokemonStats object
  function savePokemonInfo(record)
  {
    var id = record.national_id + ''

    if (!(pokemonStats.has(id)))
      pokemonStats.set(id, record)
  }

  // Display pokemon stats in popup
  function displayStats(target)
  {
    var id = target.getAttribute('pkdx-id')
    var stats = pokemonStats.get(id)
    var typesHTML = stats.types.map((type, index) =>
    {
      return stats.types[index].name
    }).join('\n')

    document.query('.stats-screen').innerHTML =
    `
      <a href="#" class="close">
        <img src="images/cross.png" alt="">
      </a>
      <img src="https://pokeapi.co/media/img/${id}.png" alt="${stats.name}">
      <p class="name">${stats.name} #${id}</p>
      <table>
        <tr>
          <th>Type
          <td>${typesHTML}
        <tr>
          <th>Attack
          <td>${stats.attack}
        <tr>
          <th>Defense
          <td>${stats.defense}
        <tr>
          <th>HP
          <td>${stats.hp}
        <tr>
          <th>SP Attack
          <td>${stats.sp_atk}
        <tr>
          <th>SP Defense
          <td>${stats.sp_def}
        <tr>
          <th>Speed
          <td>${stats.speed}
        <tr>
          <th>Weight
          <td>${stats.weight}
        <tr>
          <th>Total moves
          <td>${stats.moves.length}
      </table>
    `
  }


  // Load first 12 records on start
  addRecords()






  // Display pokemon data on click
  on(pokemonList, 'click', event =>
  {
    var card = event.target.closest('pokemon-card')

    if (card) {
      displayStats(card)
      document.query('.stats-screen').show()
    }
  })




  // Helpers.
  function on($emitter, type, handler)
  {
    $emitter.addEventListener(type, handler)
  }

  function once(onetimer)
  {
    var flag = false

    return function()
    {
      if (flag) return

      onetimer()
      flag = true
    }
  }
})()