# Pokedex #
This repo contains my implementation of test task #1 for kottans.org JS course.

You may check the result out [here](http://stasgavrylov.bitbucket.org/pokedex/)

## Features ##
* No dependencies
* Vanilla JS (ftw)
* Pretty small (JS - 13.5 KB, CSS - 7.5 KB non-minified)
* Supports filtering by union and by intersection

### Be aware ###
The project uses some experimental features and pure ES6 syntax
without transpiling.
It was not tested in any browser except latest Chrome.
Your browser should have 'Experimental ES6 features' flag enabled.
