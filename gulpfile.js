"use strict"

const process = require("child_process")
    , fs = require("fs")
    , os = require("os")
    , path = require("path")

const gulp = require("gulp")
    , postcss = require("gulp-postcss")
    , autoprefixer = require("gulp-autoprefixer")
    , concat = require("gulp-concat")
    , connect = require("gulp-connect")
    , plumber = require("gulp-plumber")

const devDependencies = require("./package.json").devDependencies

const processors = Object.keys(devDependencies)
    .filter(key => key.startsWith("postcss"))
    .map(require)

const SERVER_ROOT = "./"
    , SERVER_PORT = 8080

const GLOB_CSS = "src/styles/**/*.css"
    , GLOB_CSS_PARTIALS = "!src/styles/**/_*.css"
    , GLOB_JS = "scripts/**/*.js"
    , GLOB_HTML = "./*.html"
    , GLOB_OUTPUT = "../bitbucketrepo/pokedex/"

gulp.task("html", () =>
{
    gulp.src(GLOB_HTML)
        .pipe(connect.reload())
})

gulp.task("js", () =>
{
    gulp.src(GLOB_JS)
        .pipe(gulp.dest(GLOB_OUTPUT + '/scripts/'))
        .pipe(connect.reload())
})

gulp.task("css", () =>
{
    gulp.src([GLOB_CSS, GLOB_CSS_PARTIALS])
        .pipe(plumber())
        .pipe(postcss(processors))
        .pipe(autoprefixer())
        .pipe(gulp.dest("style"))
        .pipe(gulp.dest(GLOB_OUTPUT + '/style/'))
        .pipe(connect.reload())
})

gulp.task("watch", () =>
{
    gulp.watch(GLOB_HTML, [ "html" ])
    gulp.watch(GLOB_CSS, [ "css" ])
    gulp.watch(GLOB_JS, [ "js" ])

})

gulp.task("serve", () =>
{
    connect.server
    ({
        root: SERVER_ROOT,
        port: SERVER_PORT
    })
})

gulp.task("reload", () =>
{
    connect.server
    ({
        root: SERVER_ROOT,
        port: 9090,
        livereload: true
    })
})

gulp.task("compile", [ "css", "js", "html" ])
    .task("default", [ "compile" ])
    .task("dev", [ "reload", "compile", "watch" ])
    .task("start", [ "dev" ])